<div class="email form span12">
	<?php 
	$inputDefaults = array(
		'label' => false, 'div' => false
	);
	echo $this->Form->create('Email', array('class' => 'form-horizontal', 'inputDefaults' => $inputDefaults));
	?>
	<fieldset>
		<legend><?php echo __('Compose Email'); ?></legend>
		<div class="control-group">
			<label class="control-label" for="EmailSubject">
				<?php echo __('Subject'); ?>
			</label>
			<div class="controls">
				<?php echo $this->Form->input('subject', array('class' => 'input-block-level')); ?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="EmailText">
				<?php echo __('Text'); ?>
			</label>
			<div class="controls">
				<?php echo $this->Form->input('text', array('type' => 'textarea', 'rows' => 20, 'cols' => 50, 'class' => 'input-block-level')); ?>
			</div>
		</div>
		<div class="form-actions">
			<?php echo $this->Form->input(__('Send'), array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
		</div>
	</fieldset>
	<?php
	echo $this->Form->end();
	?>
</div>
