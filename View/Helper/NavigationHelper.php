<?php
App::uses('AppHelper', 'View/Helper');
class NavigationHelper extends AppHelper {
    
    public function activeLink($currentLocation, $targetLocation) {
    	if ($currentLocation == $targetLocation) {
    		return 'active';
    	}
    	return 'inactive';
    }

}