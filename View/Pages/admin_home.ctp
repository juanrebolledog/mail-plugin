<!-- descriptions -->
<div class="row-fluid">
	<div class="span6">
		<h2><?php echo $this->Html->link(__('Subscribers'), array('controller' => 'subscribers')); ?></h2>
		<p>
			<?php echo __('Manage Subscribers who receive the Emails this system sends'); ?>
		</p>
	</div>
	<div class="span6">
		<h2><?php echo $this->Html->link(__('Email Composer'), array('controller' => 'email', 'action' => 'compose')); ?></h2>
		<p>
			<?php echo __('Compose Emails to send to Subscribers'); ?>
		</p>
	</div>
</div>
<!-- /descriptions -->
<!-- links -->
<div class="row-fluid hidden-phone">
	<div class="span6">
		<p><?php echo $this->Html->link(__('Go'), array('admin' => true, 'controller' => 'subscribers', 'action' => 'index'), array('class' => 'btn')); ?></p>
	</div>
	<div class="span6">
		<p><?php echo $this->Html->link(__('Go'), array('admin' => true, 'controller' => 'email', 'action' => 'compose'), array('class' => 'btn')); ?></p>
	</div>
</div>
<!-- /links -->
<br><br>
<!-- descriptions -->
<div class="row-fluid">
    <div class="span6">
        <h2><?php echo $this->Html->link(__('Messages'), array('controller' => 'messages')); ?></h2>
        <p>
            <?php echo __('View past or pending messages.'); ?>
        </p>
    </div>
</div>
<!-- /descriptions -->
<!-- links -->
<div class="row-fluid hidden-phone">
    <div class="span6">
        <p><?php echo $this->Html->link(__('Go'), array('admin' => true, 'controller' => 'messages', 'action' => 'index'), array('class' => 'btn')); ?></p>
    </div>
</div>
<!-- /links -->