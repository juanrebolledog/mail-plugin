<div class="pagination pagination-centered">
    <ul>
        <?php echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), null, array('escape' => false, 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));?>
        <?php echo $this->Paginator->numbers(array('tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active', 'separator' => null));?>
        <?php echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), null, array('escape' => false, 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));?>
    </ul>
</div>