<div class="subscribers form span12">
	<?php
	$inputDefaults = array(
		'label' => false, 'div' => false, 'class' => 'input-xlarge', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline'))
	);
	echo $this->Form->create('Subscriber', array('type' => 'delete', 'class' => 'form-horizontal', 'inputDefaults' => $inputDefaults)); ?>
	<fieldset>
		<legend><?php echo __('Unsubscribe'); ?></legend>
		<?php if ($this->Form->isFieldError('Subscriber.email')) { $class = 'control-group error'; } else { $class = 'control-group'; } ?>
		<div class="<?php echo $class; ?>">
			<label class="control-label" for="SubscriberEmail">
				<?php echo __('Email'); ?>
			</label>
			<div class="controls">
				<?php echo $this->Form->input('email'); ?>
			</div>
		</div>
		<div class="form-actions">
			<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
		</div>
	</fieldset>
	<?php echo $this->Form->end();?>
</div>