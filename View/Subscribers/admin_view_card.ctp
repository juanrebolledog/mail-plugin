<div class="span2 well" style="padding: 8px 0">
    <ul class="nav nav-list">
        <li class="nav-header">
            <?php echo __('Actions'); ?>
        </li>
        <li>
            <?php echo $this->Html->link(__('List Subscribers'), array('admin' => true, 'action' => 'index')); ?>
        <li><?php echo $this->Html->link(__('View Subscriber'), array('action' => 'view', $subscriber['Subscriber']['id'])); ?></li>
        </li>
    </ul>
</div>
<div class="subscribers span10">
    <h2><?php echo __('Subscriber Card'); ?></h2>
    <hr>
    <?php echo $this->Html->image('/files/cards/' . $subscriber['Subscriber']['card_filename']); ?>
</div>