<div class="span2 well" style="padding: 8px 0">
	<ul class="nav nav-list">
		<li class="nav-header">
			<?php echo __('Actions'); ?>
		</li>
		<li>
			<?php echo $this->Html->link(__('List Subscriber'), array('admin' => true, 'action' => 'index')); ?>
		</li>
	</ul>
</div>
<div class="subscribers form span10">
    <h2><?php echo __('Subscribe'); ?></h2>
    <hr>
	<?php
	$inputDefaults = array(
		'label' => false, 'div' => false, 'class' => 'input-xlarge', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline'))
	);
	echo $this->Form->create('Subscriber', array('class' => 'form-horizontal', 'inputDefaults' => $inputDefaults)); ?>
	<fieldset>
        <?php if ($this->Form->isFieldError('Subscriber.identification_number')) { $class = 'control-group error'; } else { $class = 'control-group'; } ?>
        <div class="<?php echo $class; ?>">
            <label class="control-label" for="SubscriberEmail">
                <?php echo __('Identification Number'); ?>
            </label>
            <div class="controls">
                <?php echo $this->Form->input('identification_number'); ?>
            </div>
        </div>
		<?php if ($this->Form->isFieldError('Subscriber.email')) { $class = 'control-group error'; } else { $class = 'control-group'; } ?>
		<div class="<?php echo $class; ?>">
			<label class="control-label" for="SubscriberEmail">
				<?php echo __('Email'); ?>
			</label>
			<div class="controls">
				<?php echo $this->Form->input('email'); ?>
			</div>
		</div>
		<div class="form-actions">
			<?php echo $this->Form->input(__('Submit'), array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
		</div>
	</fieldset>
	<?php echo $this->Form->end();?>
</div>