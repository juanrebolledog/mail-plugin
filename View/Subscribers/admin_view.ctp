<div class="well span2" style="padding: 8px 0">
    <ul class="nav nav-list">
        <li class="nav-header">
            <?php echo __('Acciones'); ?>
        </li>
        <li><?php echo $this->Html->link(__('View Registration Card'), array('action' => 'view_card', $subscriber['Subscriber']['id'])); ?></li>
        <?php if ($subscriber['Subscriber']['active']): ?>
        <li><?php echo $this->Form->postLink(__('Remove Subscriber from Mailing List'), array('action' => 'unsubscribe', $subscriber['Subscriber']['id']), null, __('Are you sure you want to delete %s from the subscription list? The subscriber will STILL be enrolled in the party but will not receive emails anymore.', $subscriber['Subscriber']['first_names'] . ' ' . $subscriber['Subscriber']['last_names'])); ?> </li>
        <?php endif; ?>
        <li><?php echo $this->Form->postLink(__('Delete Subscriber from System'), array('action' => 'delete', $subscriber['Subscriber']['id']), null, __('Are you sure you want to delete %s from the subscription AND party list? The subscriber will be REMOVED from the party and will NOT receive emails anymore.', $subscriber['Subscriber']['first_names'] . ' ' . $subscriber['Subscriber']['last_names'])); ?> </li>
        <li><?php echo $this->Html->link(__('List Subscribers'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Subscriber'), array('action' => 'add')); ?> </li>
    </ul>
</div>
<div class="subscribers span10">
    <h2><?php echo __('Subscriber'); ?></h2>
    <hr>
	<dl>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($subscriber['Subscriber']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($subscriber['Subscriber']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Names'); ?></dt>
		<dd>
			<?php echo h($subscriber['Subscriber']['first_names']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Names'); ?></dt>
		<dd>
			<?php echo h($subscriber['Subscriber']['last_names']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Identification Number'); ?></dt>
		<dd>
			<?php echo h($subscriber['Voter']['nacionalidad'] . '-' . $subscriber['Subscriber']['identification_number']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Voting Center'); ?></dt>
        <dd>
            <?php echo h($subscriber['VotingCenter']['nombre_centro']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Center Address'); ?></dt>
        <dd>
            <?php echo h($subscriber['VotingCenter']['direccion_centro']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Center State'); ?></dt>
        <dd>
            <?php echo h($subscriber['VotingLocation']['estado']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Center Municipality'); ?></dt>
        <dd>
            <?php echo h($subscriber['VotingLocation']['municipio']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Center Parrish'); ?></dt>
        <dd>
            <?php echo h($subscriber['VotingLocation']['parroquia']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Receives Messages'); ?></dt>
        <dd>
            <?php echo ($subscriber['Subscriber']['active']) ? __('Yes'):__('No'); ?>
            &nbsp;
        </dd>
	</dl>
</div>