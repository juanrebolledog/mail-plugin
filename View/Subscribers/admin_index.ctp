<div class="span2 well" style="padding: 8px 0">
	<ul class="nav nav-list">
		<li class="nav-header">
			<?php echo __('Actions'); ?>
		</li>
		<li>
			<?php echo $this->Html->link(__('New Subscriber'), array('admin' => true, 'action' => 'subscribe')); ?>
		</li>
	</ul>
</div>
<div class="subscribers index span10">
	<h2><?php echo __('Subscribers');?></h2>
    <hr>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th><?php echo $this->Paginator->sort('email', __('Email'));?></th>
                <th><?php echo $this->Paginator->sort('identification_number', __('Identification Number')); ?></th>
                <th><?php echo $this->Paginator->sort('active', __('Receives Messages')); ?></th>
				<th class="hidden-phone"><?php echo $this->Paginator->sort('created', __('Created'));?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($subscribers as $subscriber): ?>
			<tr>
				<td><?php echo h($subscriber['Subscriber']['email']); ?>&nbsp;</td>
                <td><?php echo h($subscriber['Subscriber']['identification_number']); ?></td>
                <td><?php echo ($subscriber['Subscriber']['active']) ? __('Yes'):__('No'); ?></td>
				<td class="hidden-phone"><?php echo h($subscriber['Subscriber']['created']); ?>&nbsp;</td>
				<td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $subscriber['Subscriber']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $subscriber['Subscriber']['id']), null, __('Are you sure you want to delete # %s?', $subscriber['Subscriber']['id'])); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
    <?php echo $this->element('pagination'); ?>
</div>