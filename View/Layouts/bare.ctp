<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo __('Newsletter App'); ?>
        <?php echo $title_for_layout; ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->script('jquery-1.7.2');
        echo $this->Html->script('bootstrap-dropdown');
        echo $this->Html->script('bootstrap-transition');
        echo $this->Html->script('bootstrap-collapse');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    <style>
    body {
        padding-top: 90px; /* 60px to make the container go all the way to the bottom of the topbar */
        position: relative;
    }
    </style>
    <?php
    echo $this->Html->css('bootstrap-responsive.min');
    ?>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="#"><?php echo __('Newsletter Manager App'); ?></a>
                <div class="nav-collapse">
                    <ul class="nav pull-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php echo __('Language'); ?>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><?php echo $this->Html->link(__('Spanish'), array('admin' => false, 'controller' => 'utilities', 'action' => 'changeLanguage', 'spa')); ?></li>
                                <li><?php echo $this->Html->link(__('English'), array('admin' => false, 'controller' => 'utilities', 'action' => 'changeLanguage', 'eng')); ?></li>
                            </ul>                            
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <div class="container">
        <div id="content-fluid">
            <?php echo $this->Session->flash(); ?>
            <div class="row-fluid">
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>

    </div>
</body>
</html>
