<?php echo $this->fetch('content');?>

<?php 
$url = Router::url(array('admin' => false, 'controller' => 'subscribers', 'action' => 'unsubscribe'), true); 
?>

<?php echo __('If you wish to unsubscribe, go to %s', $url); ?>