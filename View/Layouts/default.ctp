<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $title_for_layout; ?> - 
        <?php echo __('Newsletter App'); ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('style');
        echo $this->Html->script('jquery-1.7.2');
        echo $this->Html->script('bootstrap-dropdown');
        echo $this->Html->script('bootstrap-transition');
        echo $this->Html->script('bootstrap-collapse');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php
        echo $this->Html->css('bootstrap-responsive.min');
    ?>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <?php echo $this->Html->link(__('Newsletter Manager App'), array('admin' => true, 'controller' => 'pages', 'action' => 'home'), array('class' => 'brand')); ?>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li class="<?php echo $this->Navigation->activeLink($this->request->params['controller'], 'pages'); ?>">
                            <?php echo $this->Html->link('<i class="icon-home icon-white"></i> '.__('Home'), array('admin' => true, 'controller' => 'pages', 'action' => 'home'), array('escape' => false)); ?>
                        </li>
                        <li class="<?php echo $this->Navigation->activeLink($this->request->params['controller'], 'subscribers'); ?>">
                            <?php echo $this->Html->link(__('Subscribers'), array('admin' => true, 'controller' => 'subscribers', 'action' => 'index')); ?>
                        </li>
                        <li class="<?php echo $this->Navigation->activeLink($this->request->params['controller'], 'email'); ?>">
                            <?php echo $this->Html->link(__('Compose Email'), array('admin' => true, 'controller' => 'email', 'action' => 'compose')); ?>
                        </li>
                        <li class="<?php echo $this->Navigation->activeLink($this->request->params['controller'], 'messages'); ?>">
                            <?php echo $this->Html->link(__('Messages'), array('admin' => true, 'controller' => 'messages')); ?>
                        </li>
                        <?php if ($this->Session->read('Auth.User')): ?>
                    </ul>
                    <ul class="nav pull-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php echo __('Language'); ?>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><?php echo $this->Html->link(__('Spanish'), array('admin' => false, 'controller' => 'utilities', 'action' => 'changeLanguage', 'spa')); ?></li>
                                <li><?php echo $this->Html->link(__('English'), array('admin' => false, 'controller' => 'utilities', 'action' => 'changeLanguage', 'eng')); ?></li>
                            </ul>                            
                        </li>
                        <li>
                            <?php echo $this->Html->link(__('Logout'), array('admin' => false, 'controller' => 'users', 'action' => 'logout')); ?>
                        </li>
                    </ul>
                    <?php endif; ?>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <div class="container" id="main-content">
        <div id="content-fluid">
            <?php echo $this->Session->flash(); ?>
            <div class="row-fluid">
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
        <hr />
        <footer>
            <p>
                <?php echo __('Newsletter Manager App'); ?>
            </p>
        </footer>
    </div>
</body>
</html>
