<h1 style="font-size: 36px; font-weight: normal; color: #333333; font-family: Georgia, 'Times New Roman', Times, serif; margin-top: 0px; margin-bottom: 20px;">
	<?php echo $msgSubject; ?>
</h1>
<p style="font-size: 16px; line-height: 22px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0px;">
    <?php echo $msgBody; ?>
    <br>
    <a style="color: #bc1f31; text-decoration: none;" href="<?php echo Router::url('http://oraporvenezuela.org/', true); ?>">Ir al sitio</a>
</p>