<h1><?php echo $msgSubject; ?></h1>

<p>
    <?php echo $msgBody; ?>
    <br>
    <h2><?php echo __('Subscriber Information'); ?></h2>
    <p><?php echo __('Name'); ?></p>
    <p><?php echo $full_name; ?></p>
    <p><?php echo __('Email'); ?></p>
    <p><?php echo $email; ?></p>
    <p><?php echo __('Identification Number'); ?></p>
    <p><?php echo $identification_number; ?></p>
    <a href="<?php echo Router::url('http://oraporvenezuela.org/', true); ?>"><?php echo __('Ir al sitio'); ?></a>
</p>