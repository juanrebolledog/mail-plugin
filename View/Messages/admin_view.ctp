<div class="actions span2 well" style="padding: 8px 0">
	<ul class="nav nav-list">
		<li class="nav-header">
			<?php echo __('Actions'); ?>
		</li>
		<li><?php echo $this->Form->postLink(__('Delete Message'), array('action' => 'delete', $message['Message']['id']), null, __('Are you sure you want to delete # %s?', $message['Message']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Messages'), array('action' => 'index')); ?> </li>
		</li>
	</ul>
</div>
<div class="users view span10">
<h2><?php  echo __('User');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($message['Message']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subject'); ?></dt>
		<dd>
			<?php echo h($message['Message']['subject']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($message['Message']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sent'); ?></dt>
		<dd>
			<?php echo $message['Message']['unsent'] ? __('Partially or Completely Unsent'):__('Completely Sent'); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($message['Message']['created']); ?>
			&nbsp;
		</dd>
	</dl>


			<h2><?php echo __('Recipients'); ?></h2>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo __('Recipient'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($message['Recipient'] as $recipient): ?>
					<tr>
						<td>
							<?php echo $recipient['Subscriber']['email']; ?>
						</td>
						<td>
							<?php echo $recipient['sent'] ? __('Sent'):__('Unsent'); ?>
							<?php if ($recipient['cancelled']): ?>
							<span class="label label-important"><?php echo __('Cancelled by Admin'); ?></span>
							<?php endif; ?>
						</td>
						<td>
							<?php if (!$recipient['sent'] && !$recipient['cancelled']): ?>
							<?php echo $this->Form->postLink(__('Delete from Queue'), array('admin' => true, 'controller' => 'messages', 'action' => 'dequeue_recipient', $recipient['id']), array('confirm' => __('Are you sure you want to stop this subscriber to receive this message?'))); ?>
							<?php else: ?>
							<?php echo $this->Form->postLink(__('Requeue'), array('admin' => true, 'controller' => 'messages', 'action' => 'requeue_recipient', $recipient['id']), array('confirm' => __('Are you sure you want to requeue this recipient?'))); ?> 
							<?php endif; ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>


</div>