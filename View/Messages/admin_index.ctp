<div class="subscribers index span12">
	<h2><?php echo __('Messages');?></h2>
    <hr>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th><?php echo $this->Paginator->sort('subject'); ?></th>
				<th><?php echo $this->Paginator->sort('sent'); ?></th>
				<th><?php echo $this->Paginator->sort('created'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($messages as $message): ?>
			<tr>
				<td><?php echo h($message['Message']['subject']); ?>&nbsp;</td>
				<td><?php echo $message['Message']['unsent'] ? __('Unsent'):__('Sent'); ?>&nbsp;</td>
				<td><?php echo h($message['Message']['created']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $message['Message']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('admin' => true, 'action' => 'delete', $message['Message']['id']), null, __('Are you sure you want to delete # %s?', $message['Message']['id'])); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php echo $this->element('pagination'); ?>
</div>