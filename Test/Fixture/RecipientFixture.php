<?php
/**
 * RecipientFixture
 *
 */
class RecipientFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'message_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'subscriber_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'sent' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'delivery_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fkParentMessage' => array('column' => 'message_id', 'unique' => 0),
			'fkMessageRecipient' => array('column' => 'subscriber_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'message_id' => 1,
			'subscriber_id' => 1,
			'sent' => 1,
			'delivery_date' => '2012-10-13 18:56:15'
		),
	);

}
