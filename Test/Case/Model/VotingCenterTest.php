<?php
App::uses('VotingCenter', 'Model');

/**
 * VotingCenter Test Case
 *
 */
class VotingCenterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.voting_center'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VotingCenter = ClassRegistry::init('VotingCenter');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VotingCenter);

		parent::tearDown();
	}

}
