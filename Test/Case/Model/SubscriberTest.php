<?php
App::uses('Subscriber', 'Model');

/**
 * Subscriber Test Case
 * @property Subscriber $Subscriber
 */
class SubscriberTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
        'app.subscriber', 'app.voter'
    );

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Subscriber = ClassRegistry::init('Subscriber');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Subscriber);

		parent::tearDown();
	}

    public function testSubscribe() {
        $data = array(
            $this->Subscriber->alias => array(
                'identification_number' => 17316036,
                'email' => 'test@example.com'
            )
        );

        $this->assertTrue($this->Subscriber->subscribe($data));
    }

    public function testSubscriberWithDuplicateData() {
        $data = array(
            $this->Subscriber->alias => array(
                'identification_number' => 17316036,
                'email' => 'test@example.com'
            )
        );
        $this->Subscriber->subscribe($data);
        $this->assertFalse($this->Subscriber->subscribe($data));
    }

    public function testSubscriberWithMalformedEmail() {
        $data = array(
            $this->Subscriber->alias => array(
                'identification_number' => 17316036,
                'email' => 'test'
            )
        );
        $this->assertFalse($this->Subscriber->subscribe($data));
    }

    public function testGenerateCard() {
        $data = array(
            $this->Subscriber->alias => array(
                'identification_number' => 18321321,
                'first_names' => 'JOSÉ ALEJANDRO',
                'last_names' => 'RODRÍGUEZ PÉREZ'
            )
        );

        $filename = $data[$this->Subscriber->alias]['identification_number'] . '.png';

        $genFilename = $this->Subscriber->generateCard($data);

        $this->assertEqual($filename, $genFilename);
    }

    public function testUnsubscribe() {
        $data = array(
            $this->Subscriber->alias => array(
                'identification_number' => 17316036,
                'email' => 'test@example.com'
            )
        );
        $this->Subscriber->subscribe($data);

        $result = $this->Subscriber->unsubscribe($data[$this->Subscriber->alias]['email']);
        $this->assertTrue($result);
    }

    public function testRemoveSubscriber() {
        $id = 1;
        $result = $this->Subscriber->removeSubscriber($id);
        $this->assertTrue($result);
    }

}
