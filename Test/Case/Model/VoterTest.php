<?php
App::uses('Voter', 'Model');

/**
 * Voter Test Case
 *
 */
class VoterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.voter'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Voter = ClassRegistry::init('Voter');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Voter);

		parent::tearDown();
	}

}
