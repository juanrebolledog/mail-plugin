<?php
App::uses('VotingLocation', 'Model');

/**
 * VotingLocation Test Case
 *
 */
class VotingLocationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.voting_location'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VotingLocation = ClassRegistry::init('VotingLocation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VotingLocation);

		parent::tearDown();
	}

}
