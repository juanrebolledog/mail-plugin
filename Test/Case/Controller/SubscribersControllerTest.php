<?php
App::uses('SubscribersController', 'Controller');

/**
 * TestSubscribersController
 * @property Subscribers $Subscribers
 */
class TestSubscribersController extends SubscribersController {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * SubscribersController Test Case
 *
 */
class SubscribersControllerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.subscriber');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Subscribers = new TestSubscribersController();
		$this->Subscribers->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Subscribers);

		parent::tearDown();
	}

    public function testSendWelcomeEmail() {
        $subscriber = array(
            'Subscriber' => array(
                'id' => 1,
                'email' => 'juanrebolledog@gmail.com',
                'first_names' => 'Juan Francisco',
                'last_names' => 'Rebolledo Gómez',
                'card_filename' => '17316036.png',
                'nationality' => 'V'
            )
        );
        $this->assertTrue($this->Subscribers->sendWelcomeEmail($subscriber));
    }

    public function testNotifiyOfNewSubscriber() {
        $subscriber = array(
            'Subscriber' => array(
                'id' => 1,
                'email' => 'juanrebolledog@gmail.com',
                'first_names' => 'Juan Francisco',
                'last_names' => 'Rebolledo Gómez',
                'identification_number' => '17316036',
                'card_filename' => '17316036.png',
                'nationality' => 'V'
            )
        );
        $this->assertTrue($this->Subscribers->notifyOfNewSubscriber($subscriber));
    }

}
