<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Emails Controller
 * @property Message $Message
 * @property Subscriber $Subscriber
 */
class EmailController extends AppController {

	public function admin_compose() {
		if ($this->request->is('post')) {
			if ($this->send($this->request->data)) {
				$this->Session->setFlash(__('Sending emails to all subscribers. This may take a while depending on the number of subscribers to this list.'), 'flash_success');
				$this->redirect('/');
			}
		}
	}

	private function send($data) {
		$this->Message = ClassRegistry::init('Message');
		$message = array(
			'Message' => array(
				'subject' => $data['Email']['subject'],
				'body' => $data['Email']['text']
			)
		);
		$this->Subscriber = ClassRegistry::init('Subscriber');
		$subscribers = $this->Subscriber->find('all', array(
            'conditions' => array(
                'active' => true
            )
        ));
		if (!empty($subscribers)) {
			$message['Recipient'] = array();
			foreach ($subscribers as $subscriber) {
				$recipient = array(
					'subscriber_id' => $subscriber['Subscriber']['id']
				);
				array_push($message['Recipient'], $recipient);
			}
		}
		$this->Message->saveAssociated($message);
		return true;
	}

}
