<?php
App::uses('AppController', 'Controller');
/**
 * Utilities Controller
 *
 */
class UtilitiesController extends AppController {

	public $languages = array('spa', 'eng');

	public function changeLanguage($lang) {
		if (in_array($lang, $this->languages)) {
			$this->Session->write('Config.language', $lang);
			$this->redirect($this->request->referer());
		}
	}

}
