<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Subscribers Controller
 * @property Subscriber $Subscriber
 */
class SubscribersController extends AppController {

	public function admin_index() {
		$this->paginate = array(
			'Subscriber' => array(
				'limit' => 15
			)
		);
		$subscribers = $this->paginate('Subscriber');
		$this->set(compact('subscribers'));
	}

    public function admin_view($id) {
        $subscriber = $this->Subscriber->view($id);
        $this->set(compact('subscriber'));
    }

	public function admin_delete($id) {
		$this->Subscriber->id = $id;
		if (!$this->Subscriber->exists()) {
			throw new NotFoundException(__('No such Subscriber'));
		}
		if ($this->Subscriber->removeSubscriber($id)) {
			$this->Session->setFlash(__('Subscriber deleted'), 'flash_success');
			$this->redirect(array('admin' => true, 'controller' => 'subscribers', 'action' => 'index'));
		}
	}

	public function admin_subscribe() {
		if ($this->request->is('post')) {
			if ($this->Subscriber->subscribe($this->request->data)) {
                $subscriber = $this->Subscriber->view($this->Subscriber->getLastInsertID());
                $this->sendWelcomeEmail($subscriber);
				$this->Session->setFlash(__('The email has been subscribed to our newsletter'), 'flash_success');
				$this->redirect(array('admin' => true, 'controller' => 'subscribers', 'action' => 'index'));
			} else {
				$this->Session->setFlash(__('There was a problem subscribing the email to our newsletter'), 'flash_error');
			}
		}
	}

    public function admin_unsubscribe($id) {
        if ($this->request->is('post')) {
            $subscriber = $this->Subscriber->view($id);
            if ($this->Subscriber->unsubscribe($subscriber[$this->Subscriber->alias]['email'])) {
                $this->Session->setFlash(__('The email has been unsubscribed from our newsletter'), 'flash_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('There was a problem unsubscribing the email from our newsletter'), 'flash_error');
                $this->redirect(array('action' => 'view', $id));
            }
        }
    }

	public function subscribe() {
		$this->layout = 'bare';
		if ($this->request->is('post')) {
			if ($this->Subscriber->subscribe($this->request->data)) {
                if ($this->request->is('ajax')) {
                    $this->layout = null;
                    $response = array(
                        'result' => true
                    );
                    $this->set('response', $response);
                    $this->set('_serialize', json_encode(true));
                } else {
                    $this->Session->setFlash(__('You have been subscribed to our newsletter'), 'flash_success');
                    $this->redirect(Configure::read('App.website'));
                }
                $subscriber = $this->Subscriber->view($this->Subscriber->getLastInsertID());
                $this->sendWelcomeEmail($subscriber);
                $this->notifyOfNewSubscriber($subscriber);
			} else {
                $response = array(
                    'result' => false,
                    'invalid_fields' => $this->Subscriber->invalidFields()
                );
                $this->set('response', $response);
                $this->set('_serialize', array('response'));
                $this->Session->setFlash(__('There was a problem subscribing you to our newsletter'), 'flash_error');
			}
		}
	}

	public function unsubscribe() {
		$this->layout = 'bare';
		if ($this->request->is('delete')) {
            try {
                $result = $this->Subscriber->unsubscribe($this->request->data[$this->Subscriber->alias]['email']);
                if ($result) {
                    $this->Session->setFlash(__('You were successfully unsubscribed from our newsletter, you will not be receiving any mail from us.'), 'flash_success');
                } else {
                    $this->Session->setFlash(__('There was a problem unsubscribing this email from our newsletter'), 'flash_error');
                }
            } catch (NotFoundException $e) {
                $this->Session->setFlash(__('This email does not seem to be in our database.'), 'flash_error');
            }
		}
	}

    public function admin_view_card($id) {
        $subscriber = $this->Subscriber->view($id);
        $this->set(compact('subscriber'));
    }

    public function sendWelcomeEmail($subscriber) {
        $message = __('Welcome to ORA, you have been added to our mailing list and party list. Attached in this email you will find a registration card for your records.');
        $subject = __('Welcome to ORA');
        $body = $message;
        $email = new CakeEmail('smtp');
        $tplData = array(
            'msgSubject' => $subject,
            'msgBody' => $body
        );
        $bcc = array(
            $subscriber['Subscriber']['email'] => $subscriber['Subscriber']['first_names'] . ' ' . $subscriber['Subscriber']['last_names']
        );
        $email->bcc($bcc);
        $email->attachments(WWW_ROOT . 'files' . DS . 'cards' . DS . $subscriber['Subscriber']['card_filename']);
        $email->viewVars($tplData);
        $email->subject($subject);
        $email->template('newsletter');
        $email->emailFormat('both');
        $email->helpers(array('Html'));
        $r = $email->send();
        if ($r) {
            return true;
        }
        return false;
    }

    public function notifyOfNewSubscriber($subscriber) {
        $message = __('This is a notification of a new subscriber in the list. You will find the registration card attached to this email for your records.');
        $subject = __('[ORA] New subscriber');
        $body = $message;
        $email = new CakeEmail('smtp');
        $tplData = array(
            'msgSubject' => $subject,
            'msgBody' => $body,
            'full_name' => $subscriber['Subscriber']['first_names'] . ' ' . $subscriber['Subscriber']['last_names'],
            'identification_number' => $subscriber['Subscriber']['nationality'] . '-' . $subscriber['Subscriber']['identification_number'],
            'email' => $subscriber['Subscriber']['email'],
            'id' => $subscriber['Subscriber']['id']
        );
        $bcc = Configure::read('App.admin_email');
        $email->bcc($bcc);
        $email->attachments(WWW_ROOT . 'files' . DS . 'cards' . DS . $subscriber['Subscriber']['card_filename']);
        $email->viewVars($tplData);
        $email->subject($subject);
        $email->template('default');
        $email->emailFormat('both');
        $email->helpers(array('Html'));
        $r = $email->send();
        if ($r) {
            return true;
        }
        return false;
    }

}
