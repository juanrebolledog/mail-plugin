<?php
App::uses('AppController', 'Controller');
/**
 * Messages Controller
 * @property Message $Message
 */
class MessagesController extends AppController {

    /**
     * Admin Index
     *
     * @return void
     * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
     */
    public function admin_index() {
        $this->paginate = array(
            'Message' => array(
                'contain' => array('Recipient' => array('Subscriber')),
                'limit' => 20
            )
        );
        $messages = $this->paginate('Message');
        $this->set(compact('messages'));
    }

    /**
     * Admin View
     *
     * @param integer $id
     * @return void
     * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
     */
    public function admin_view($id) {
        $this->Message->id = $id;
        if (!$this->Message->exists()) {
            $this->Session->setFlash(__('No such Message'));
            $this->redirect($this->referer());
        }
        $message = $this->Message->find('first', array(
            'conditions' => array('Message.id' => $id),
            'contain' => array('Recipient' => array('Subscriber'))
        ));
        $this->set(compact('message'));
    }

    /**
     * Admin Delete
     *
     * @param integer $id
     * @return void
     * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
     */
    public function admin_delete($id) {
        if ($this->request->is('post')) {
            $this->Message->id = $id;
            if (!$this->Message->exists()) {
                $this->Session->setFlash(__('There is no such message'));
            }
            if ($this->Message->delete($id)) {
                $this->Session->setFlash(__('The message is deleted'));
            } else {
                $this->Session->setFlash(__('There was a problem deleting the message'));
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    /**
     * Admin Requeue Recipient
     *
     * @param integer $recipient_id
     * @return void
     * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
     */
    public function admin_requeue_recipient($recipient_id) {
        if ($this->request->is('post')) {
            $this->Message->Recipient->id = $recipient_id;
            if ($this->Message->Recipient->exists()) {
                if ($this->Message->Recipient->requeue($recipient_id)) {
                    $this->Session->setFlash(__('The subscriber will now receive the message'));
                } else {
                    $this->Session->setFlash(__('There was a problem requeuing this recipient'));
                }
            } else {
                $this->Session->setFlash(__('There are no elements matching your request'));
            }
        }
        $this->redirect(array('action' => 'view', $recipient_id));
    }

    /**
     * Admin Dequeue Recipient
     *
     * @param integer $recipient_id
     * @return void
     * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
     */
    public function admin_dequeue_recipient($recipient_id) {
        if ($this->request->is('post')) {
            $this->Message->Recipient->id = $recipient_id;
            if ($this->Message->Recipient->exists()) {
                if ($this->Message->Recipient->dequeue($recipient_id)) {
                    $this->Session->setFlash(__('The subscriber will no longer receive the message'));
                } else {
                    $this->Session->setFlash(__('There was a problem deleteing this recipient'));
                }
            } else {
                $this->Session->setFlash(__('There are no elements matching your request'));
            }
        }
        $this->redirect(array('action' => 'view', $recipient_id));
    }
}
