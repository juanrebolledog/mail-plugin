<?php
class HeraldShell extends AppShell {

    /**
     * Uses
     *
     * @var array
     */
    public $uses = array('Message');

    /**
     * Main
     *
     * @return void
     * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
     */
    public function main() {
        $unsent_messages = $this->Message->find('all', array(
            'conditions' => array(
                'Message.unsent_count >' => 0
            ),
            'contain' => false
        ));
        if (empty($unsent_messages)) {
            $this->out('No messages in queue');
        } else {
            $this->out('Messages in queue');
            foreach ($unsent_messages as $message) {
                $this->out('Sending Message ID: ');
                $this->out($message['Message']['id']);
                $this->Message->send($message['Message']['id']);
            }
        }
        $this->out('Exiting');
    }

}