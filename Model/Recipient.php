<?php
App::uses('AppModel', 'Model');
/**
 * Recipient Model
 *
 * @property Message $Message
 * @property Subscriber $Subscriber
 */
class Recipient extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'message_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'subscriber_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sent' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'delivery_date' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Message' => array(
			'className' => 'Message',
			'foreignKey' => 'message_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'counterCache' => array(
                'unsent_count' => array('Recipient.sent' => 0),
                'sent_count' => array('Recipient.sent' => 1),
                'cancelled_count' => array('Recipient.cancelled' => 1),
                'message_count' => true
            )
		),
		'Subscriber' => array(
			'className' => 'Subscriber',
			'foreignKey' => 'subscriber_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * Dequeue
	 *
	 * @param integer $id
	 * @return boolean true on successful dequeing
	 * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
	 */
	public function dequeue($id) {
		$this->id = $id;
		if ($this->saveField('cancelled', true)) {
			return true;
		}
		return false;
	}

	/**
	 * Requeue
	 *
	 * @param integer $id
	 * @return boolean true on successful requeing
	 * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
	 */
	public function requeue($id) {
		$this->id = $id;
		if ($this->saveField('cancelled', false) && $this->saveField('sent', false)) {
			return true;
		}
		return false;
	}

	/**
	 * Mark Sent
	 *
	 * @param array ids
	 * @return boolean on success
	 * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
	 */
	public function markSent($ids) {
		foreach ($ids as $id) {
			$this->id = $id;
			$this->saveField('sent', true);
			$this->id = null;
		}
		return true;
	}
}
