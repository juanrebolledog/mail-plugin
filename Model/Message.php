<?php
App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');
/**
 * Message Model
 *
 * @property Recipient $Recipient
 */
class Message extends AppModel {

    /**
     * undocumented class variable
     *
     * @var string
     **/
    public $virtualFields = array(
        'unsent' => 'Message.message_count - Message.sent_count'
    );

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'subject' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'body' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'sent' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Recipient' => array(
            'className' => 'Recipient',
            'foreignKey' => 'message_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
        ),
    );

    /**
     * Send
     *
     * @param integer $id
     * @return void
     * @author Juan Francisco Rebolledo Gómez <juanrebolledog@gmail.com>
     */
    public function send($id) {
        $message = $this->find('first', array(
            'conditions' => array(
                'Message.id' => $id,
                'Message.unsent_count >' => 0
            ),
            'contain' => array(
                'Recipient' => array(
                    'Subscriber'
                )
            )
        ));
        if (!empty($message)) {
            $subject = $message['Message']['subject'];
            $body = $message['Message']['body'];
            $recipientLimit = Configure::read('Email.recipientLimit');
            $recipients = $message['Recipient'];
            $recipient_size = count($recipients);
            $send_ops = ceil($recipient_size/$recipientLimit);
            for ($i=0; $i<$send_ops; $i++) {
                $email = new CakeEmail('smtp');
                $bcc = array();
                $recipient_ids = array();
                $offset = $recipientLimit*$i;
                $subscribers = array_slice($recipients, $offset, $recipientLimit);
                foreach ($subscribers as $subscriber) {
                    array_push($bcc, $subscriber['Subscriber']['email']);
                    array_push($recipient_ids, $subscriber['id']);
                }
                $tplData = array(
                    'msgSubject' => $subject,
                    'msgBody' => $body
                );
                $email->bcc($bcc);
                $email->viewVars($tplData);
                $email->subject($subject);
                $email->template('newsletter');
                $email->emailFormat('both');
                $email->helpers(array('Html'));
                $r = $email->send();
                if ($r) {
                    $this->Recipient->markSent($recipient_ids);
                }
            }
        }
    }

}
