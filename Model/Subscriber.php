<?php
App::uses('AppModel', 'Model');
App::uses('Security', 'Utility');
App::uses('Router', 'Core');
/**
 * Subscriber Model
 * @property Voter $Voter
 * @property VotingCenter $VotingCenter
 * @property VotingLocation $VotingLocation
 */
class Subscriber extends AppModel {

    public function __construct() {
        parent::__construct();
        $this->cardDirectory = WWW_ROOT . 'files' . DS . 'cards' . DS;
    }

    /**
     * Validation rules
     *
     * @var array
     */
	public $validate = array(
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Must be a valid email address',
			),
			'unique' => array(
				'rule' => array('isUnique'),
				'message' => 'The email address must be unique (not already in our database)'
			)
		),
        'identification_number' => array(
            'unique' => array(
                'rule' => array('isUnique'),
                'message' => 'The identification number must be unique (not already in our database)'
            )
        )
	);

    public function view($id) {
        $this->Voter = ClassRegistry::init('Voter');
        $this->VotingLocation = ClassRegistry::init('VotingLocation');
        $this->VotingCenter = ClassRegistry::init('VotingCenter');

        $subscriber = $this->find('first', array(
            'conditions' => array('Subscriber.id' => $id)
        ));

        $voter = $this->Voter->find('first', array(
            'conditions' => array(
                'Voter.cedula' => $subscriber[$this->alias]['identification_number']
            )
        ));

        $subscriber[$this->alias]['nationality'] = $voter[$this->Voter->alias]['nacionalidad'];

        $votingCenter = $this->VotingCenter->find('first', array(
            'conditions' => array(
                'cod_centro' => $voter['Voter']['cod_centro']
            )
        ));

        $votingLocation = $this->VotingLocation->find('first', array(
            'conditions' => array(
                'cod_estado' => $voter['Voter']['cod_estado'],
                'cod_municipio' => $voter['Voter']['cod_municipio'],
                'cod_parroquia' => $voter['Voter']['cod_parroquia'],
            )
        ));

        $result = array(
            $this->alias => $subscriber[$this->alias],
            $this->Voter->alias => $voter[$this->Voter->alias],
            $this->VotingLocation->alias => $votingLocation[$this->VotingLocation->alias],
            $this->VotingCenter->alias => $votingCenter[$this->VotingCenter->alias]
        );

        return $result;
    }

    public function subscribe($data) {
        $this->Voter = ClassRegistry::init('Voter');
        $voter = $this->Voter->find('first', array(
            'conditions' => array(
                'Voter.cedula' => $data[$this->alias]['identification_number']
            )
        ));

        $subscriber = array(
            $this->alias => array(
                'email' => $data[$this->alias]['email'],
                'unsubscribe_token' => Security::hash($data[$this->alias]['email'], null, true),
                'first_names' => $voter[$this->Voter->alias]['primer_nombre'] . ' ' . $voter[$this->Voter->alias]['segundo_nombre'],
                'last_names' => $voter[$this->Voter->alias]['primer_apellido'] . ' ' . $voter[$this->Voter->alias]['segundo_apellido'],
                'identification_number' => $voter[$this->Voter->alias]['cedula'],
                'nationality' => $voter[$this->Voter->alias]['nacionalidad']
            )
        );

        if ($filename = $this->generateCard($subscriber)) {
            $subscriber[$this->alias]['card_filename'] = $filename;
            if ($this->save($subscriber)) {
                return true;
            } else {
                $this->deleteCard($subscriber);
                return false;
            }
        }

        return false;
    }

    public function unsubscribe($email) {
        $subscriber = $this->find('first', array('conditions' => array($this->alias . '.email' => $email)));
        if (!empty($subscriber)) {
            $this->id = $subscriber[$this->alias]['id'];
            if ($this->saveField('active', 0)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function generateCard($subscriber) {
        $im = imagecreatefromjpeg(WWW_ROOT . 'files' . DS . 'template_card.jpg');
        $black = imagecolorallocate($im, 0, 0, 0);

        $name = $subscriber[$this->alias]['first_names'] . ' ' . $subscriber[$this->alias]['last_names'];
        $id_number = $subscriber[$this->alias]['nationality'] . '-' . $subscriber[$this->alias]['identification_number'];
        $font = WWW_ROOT . 'files/arial.ttf';

        imagettftext($im, 20, 0, 380, 370, $black, $font, $name);
        imagettftext($im, 20, 0, 380, 480, $black, $font, $id_number);
        $filename_path = $this->cardDirectory;
        $filename = $subscriber[$this->alias]['identification_number'] . '.png';
        imagepng($im, $filename_path . $filename);
        imagedestroy($im);
        return $filename;
    }

    public function removeSubscriber($id) {
        $subscriber = $this->find('first', array('conditions' => array($this->alias . '.id' => $id)));
        if ($this->delete($id)) {
            $this->deleteCard($subscriber);
            return true;
        }
        return false;
    }

    public function deleteCard($subscriber) {
        @unlink($this->cardDirectory . $subscriber[$this->alias]['card_filename']);
        return true;
    }
}
